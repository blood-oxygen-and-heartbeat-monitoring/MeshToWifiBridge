//************************************************************
// this is a simple example that uses the painlessMesh library to
// connect to a another network and relay messages from a MQTT broker to the nodes of the mesh network.
// To send a message to a mesh node, you can publish it to "painlessMesh/to/12345678" where 12345678 equals the nodeId.
// To broadcast a message to all nodes in the mesh you can publish it to "painlessMesh/to/broadcast".
// When you publish "getNodes" to "painlessMesh/to/gateway" you receive the mesh topology as JSON
// Every message from the mesh which is send to the gateway node will be published to "painlessMesh/from/12345678" where 12345678 
// is the nodeId from which the packet was send.
//************************************************************

#include <Arduino.h>
#include <painlessMesh.h>
#include <PubSubClient.h>
#include <WiFiClient.h>

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

#define   STATION_SSID     ""
#define   STATION_PASSWORD ""

#define HOSTNAME "MQTT_Bridge"

// Prototypes
void receivedCallback( const uint32_t &from, const String &msg );
void mqttCallback(char* topic, byte* payload, unsigned int length);

IPAddress getlocalIP();

IPAddress myIP(0,0,0,0);
IPAddress mqttBroker(192, 168, 0, 120);

painlessMesh  mesh;
WiFiClient wifiClient;
PubSubClient mqttClient(mqttBroker, 1883, mqttCallback, wifiClient);

void setup() {
  Serial.begin(115200);

  mesh.setDebugMsgTypes( ERROR | STARTUP | CONNECTION );  // set before init() so that you can see startup messages

  // Channel set to 6. Make sure to use the same channel for your mesh and for you other
  // network (STATION_SSID)
  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP_STA, 6 );
  mesh.onReceive(&receivedCallback);

  mesh.stationManual(STATION_SSID, STATION_PASSWORD);
  mesh.setHostname(HOSTNAME);

  // Bridge node, should (in most cases) be a root node. See [the wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation) for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);
}

void loop() {
  mesh.update();
  mqttClient.loop();

  if(myIP != getlocalIP()){
    myIP = getlocalIP();
    Serial.println("My IP is " + myIP.toString());

    if (mqttClient.connect("painlessMeshClient")) {
      mqttClient.publish("painlessMesh/from/gateway","Ready! ");
      mqttClient.subscribe("painlessMesh/to/#");  // 16 characters long
      Serial.println("Connected to MQTT Broker " + mqttBroker.toString());
    } else {
      Serial.println("MQTT Broker " + mqttBroker.toString()+ " not found!");
    }
  }
}

/**
 * @brief  Extracts the message coming from one of the nodes in the mesh network
 * and sends it to a topic of the mqtt broker (containing  the node id)
 * 
 * @param from the id of the node the message is coming from
 * @param msg  the message
 */
void receivedCallback( const uint32_t &from, const String &msg ) {
  
  String topic = "painlessMesh/from/" + String(from);
  Serial.printf("bridge: Received from %u msg=%s to topic %s\n", from, msg.c_str(),topic.c_str());
  mqttClient.publish(topic.c_str(), msg.c_str());
}

/**
 * @brief painlessMesh/to/#  the topic prefix (the first 16 characters) defined for incoming messages from mqtt broker
 * # can be either "gateway" which returns the list of node ids currently registered in the mesh or
 * "broadcast" which sends the message as it is into the mesh. This can be used to send information to a specivic node.
 * 
 * @param topic 
 * @param payload 
 * @param length 
 */
void mqttCallback(char* topic, uint8_t* payload, unsigned int length) {
  char* cleanPayload = (char*)malloc(length+1);
  payload[length] = '\0';
  memcpy(cleanPayload, payload, length+1);
  String msg = String(cleanPayload);
  free(cleanPayload);

  String targetStr = String(topic).substring(16);
  Serial.printf("bridge: Received msg=%s from topic %s\n", msg.c_str(),String(topic).c_str());
  if(targetStr == "gateway") // topic was painlessMesh/to/gateway
  {
    if(msg == "getNodes") {
      auto nodes = mesh.getNodeList(true);
      String str;
      for (auto &&id : nodes)
        str += String(id) + String(" ");
      mqttClient.publish("painlessMesh/from/gateway", str.c_str());
    } else if(msg == "getLayout") {
      mqttClient.publish("painlessMesh/from/gateway", mesh.subConnectionJson(true).c_str());
    } else if(msg == "getIP") {
      char GW_IP[16];
      sprintf(GW_IP,"%u.%u.%u.%u", myIP._address.bytes[0], myIP._address.bytes[1], myIP._address.bytes[2], myIP._address.bytes[3]);
      mqttClient.publish("painlessMesh/from/gateway", GW_IP);
    }else {
      mqttClient.publish("painlessMesh/from/gateway", "Not supported!");
    }
  }
  else if(targetStr == "broadcast")  // topic was painlessMesh/to/broadcast
  {
    mesh.sendBroadcast(msg);
    mqttClient.publish("painlessMesh/from/gateway", "Message broadcasted!");
  }
  else                               // topic was painlessMesh/to/1234445
  {
    uint32_t target = strtoul(targetStr.c_str(), NULL, 10);
    if(mesh.isConnected(target))
    {
      mesh.sendSingle(target, msg);
      mqttClient.publish("painlessMesh/from/gateway", "Message sended to node!");
    }
    else
    {
      mqttClient.publish("painlessMesh/from/gateway", "Client not connected!");
    }
  }
}

IPAddress getlocalIP() {
  return IPAddress(mesh.getStationIP());
}
